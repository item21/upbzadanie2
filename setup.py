from setuptools import setup

setup(
    name='cliPy',
    version='0.1.0',
    packages=['cliPy'],
    entry_points={
        'console_scripts': [
            'cliPy = cliPy.main:main'
        ]
    })
