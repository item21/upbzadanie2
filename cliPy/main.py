import argparse
from cliPy.cli.CryptoClass import CryptoClass


def main():
    parser = argparse.ArgumentParser(description='cliPy app description')

    parser.add_argument('-i', '--infile', metavar='', required=True, help="Input file")
    parser.add_argument('-k', '--keyfile', metavar='', required=True, help="Key file")
    parser.add_argument('-o', '--outfile', metavar='', help="Output file")

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-e', '--encrypt', action='store_true', help="encrypt functionality")
    group.add_argument('-d', '--decrypt', action='store_true', help="decrypt functionality")
    args = parser.parse_args()

    if args.encrypt:
        if args.outfile:
            CryptoClass.encrypt(args.infile, args.keyfile, args.outfile)
        else:
            CryptoClass.encrypt(args.infile, args.keyfile)
    elif args.decrypt:
        if args.outfile:
            CryptoClass.decrypt(args.keyfile, args.infile, args.outfile)
        else:
            CryptoClass.decrypt(args.keyfile, args.infile)


if __name__ == '__main__':
    main()
