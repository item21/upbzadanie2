import os
import sys
import timeit
from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto.Random import get_random_bytes


def validation(filename):
    if not os.path.exists(filename):
        print('File: ' + filename + ' does\'nt exists')
        return False
    elif os.path.isdir(filename):
        print('Input can\'t be directory')
        return False

    return True


class CryptoClass:
    @staticmethod
    def encrypt(in_filename, key_file, out_filename=None, chunksize=64 * 1024):
        if not out_filename:
            out_filename = in_filename + '.enc'

        if not validation(in_filename):
            sys.exit()
        
        key = os.urandom(16)

        firsthalf = get_random_bytes(8)

        counter = Counter.new(64, firsthalf)

        encryptor = AES.new(key, AES.MODE_CTR, counter=counter)

        with open(in_filename, 'rb') as infile:
            with open(out_filename, 'wb') as outfile:
                starttime = timeit.default_timer()
                outfile.write(firsthalf)

                with open(key_file, 'wb') as keyFile:
                    keyFile.write(key)

                while True:
                    chunk = infile.read(chunksize)
                    if len(chunk) == 0:
                        break
                    outfile.write(encryptor.encrypt(chunk))

                print("Encryption Completed in ", timeit.default_timer() - starttime)

    @staticmethod
    def decrypt(key_file, in_filename, out_filename=None, chunksize=64 * 1024):
        if not out_filename:
            out_filename = os.path.splitext(in_filename)[0]

        if not validation(key_file) or not validation(in_filename):
            sys.exit()

        with open(in_filename, 'rb') as infile:
            with open(key_file, 'rb') as keyFile:
                key = keyFile.read()
                if len(key) != 16:
                    print("Decryption incomplete - Invalid key length")
                    sys.exit()

            firsthalf = infile.read(8)

            iv = Counter.new(64, firsthalf)

            decryptor = AES.new(key, AES.MODE_CTR, counter=iv)

            with open(out_filename, 'wb') as outfile:
                starttime = timeit.default_timer()
                while True:
                    chunk = infile.read(chunksize)
                    if len(chunk) == 0:
                        break
                    outfile.write(decryptor.decrypt(chunk))
            print("Decryption Completed in ", timeit.default_timer() - starttime)


